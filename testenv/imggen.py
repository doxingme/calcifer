The below code will fetch a random post from the memes subreddit. Currently it picks a random submission from the top 10 posts from the hot section.https://stackoverflow.com/questions/49881935/how-do-i-get-a-random-subreddit-image-to-my-discord-py-bot

reddit = praw.Reddit(client_id='CLIENT_ID HERE',
                     client_secret='CLIENT_SECRET HERE',
                     user_agent='USER_AGENT HERE')
@bot.command()
async def meme():
    memes_submissions = reddit.subreddit('memes').hot()
    post_to_pick = random.randint(1, 10)
    for i in range(0, post_to_pick):
        submission = next(x for x in memes_submissions if not x.stickied)

    await bot.say(submission.url)

bot.run('TOKEN')


''' So i used this code to create my own variation below'''
'''So reddit has an api, so each time reddit.[anything] is used, its pulling from the api likely'''