'''Ip search command improvement test'''

@client.command(pass_context=True)
async def ipsrch(ctx, arg1):
    url = "http://ip-api.com/json/{}".format(arg1)
    async with aiohttp.ClientSession() as session:
            raw_response = await session.get(url)
            response = await raw_response.text()
            r = json.loads(response)
            await client.say("IP: "+r['query']+"\nISP: "+r['isp']+"\nCountry: "+r['countrycode']+"\nState: "+r['region']+"\nCoordinates: "+r['lat']+", "+r['lon']+"\nArea Code"+r['zip']+)





'''Embed Version of IPSearch.
Format : fg.add_field(name="Bot?", value=user.bot)
'''
@client.command(pass_context=True)
async def ipsch(ctx, arg1):
    url = "http://ip-api.com/json/{}".format(arg1)
    async with aiohttp.ClientSession() as session:
            raw_response = await session.get(url)
            response = await raw_response.text()
            r = json.loads(response)
    x = response['lat']+", "+response['lon']
    ip = discord.Embed(
        colour = discord.Colour.teal()
    )
    ip.set_author(name="IP Address Information")
    ip.add_field(name="IP", value=response['query'], inline=False)
    ip.add_field(name="ISP", value=response['isp'], inline=False)
    ip.add_field(name="Country", value=response['countrycode'], inline=False)
    ip.add_field(name="State", value=response['regionName'], inline=False)
    ip.add_field(name="Area Code", value=response['zip'], inline=False)
    ip.add_field(name="Coordinates", value=x, inline=False)
    ip.add_field(name="Mobile?", value=response['mobile'], inline=False)
    ip.add_field(name="Proxy?", value=response['proxy'], inline=False)
    await client.say(embed=ip)